<?php

class UserModel extends Model
{

    public function register()
    {
        // Sanitize Post
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        // Check if fields contain data
        if ($post['submit']) {
            if ($post['name'] == '' || $post['email'] == '' || $post['password'] == '') {
                Messages::setMsg('Please Fill In All Fields', 'error');
                return;
            }
            // Check if email is available
            $this->query('SELECT email FROM users WHERE email ="' . $post['email'] . '"');
            $row = $this->single();
            if ($row) {
                Messages::setMsg('The email entered has already been registered', 'error');
                return;
            }
            // Insert into MySQL                        
            $this->query('INSERT INTO users (name, email, password) VALUES (:name, :email, :password)');
            $this->bind(':name', $post['name']);
            $this->bind(':email', $post['email']);
            $this->bind(':password', md5($post['password']));
            $this->execute();
            // Verify
            if ($this->lastInsertId()) {
                // Redirect
                header('Location: ' . ROOT_URL . 'users/login');
            }
        }
        return;
    }

    public function login()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        //
        if ($post['submitLOGIN']) {
            // Compare Login 
            $this->query('SELECT id, name, email, password FROM users WHERE email = :email and password = :password');
            $this->bind(':email', $post['email']);
            $this->bind(':password', md5($post['password']));

            $row = $this->single();

            if ($row) {
                if (!empty($_POST["remember"])) {
                    setcookie("member_login", $post['email'], time() + (10 * 365 * 24 * 60 * 60));
                    setcookie("member_password", $post['password'], time() + (10 * 365 * 24 * 60 * 60));
                } else {
                    if (isset($_COOKIE["member_login"])) {
                        setcookie("member_login", "");
                    }
                    if (isset($_COOKIE["member_password"])) {
                        setcookie("member_password", "");
                    }
                }
                $_SESSION['is_logged_in'] = true;
                $_SESSION['user_data'] = array(
                    "id" => $row['id'],
                    "name" => $row['name'],
                    "email" => $row['email'],
                    "admin" => $row['admin']
                );
                header('Location: ' . ROOT_URL . 'blog');
            } else {
                Messages::setMsg('Incorrect Login', 'error');
            }
        }
        return;
    }

}
