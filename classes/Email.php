<?php

/**
 * Description of Email
 *
 * @author wprorok
 */
class Email
{
    private $sender;
    private $recipients;
    private $subject;
    private $body;

    function __construct($sender)
    {
        $this->sender = $sender;
        $this->recipients = array();
    }

    public function addRecipients($recipient)
    {
        array_push($this->recipients, $recipient);
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function sendEmail()
    {
        foreach ($this->recipients as $recipient) {
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: ' . $this->sender . "\r\n";
            $result = mail($recipient, $this->subject, $this->body, $headers);

            if (isset($result)) {
                Messages::setMsg("Email was SENT successfully to $recipient <br />", 'success');
                return;
            } else {
                Messages::setMsg("Email FAILED to $recipient <br />", 'error');
                return;
            }
        }
    }

}
