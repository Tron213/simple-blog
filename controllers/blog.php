<?php

class blog extends Controller
{

    protected function index()
    {
        $viewmodel = new BlogModel();
        $this->ReturnView($viewmodel->index(), true);
    }

    protected function add()
    {
        if (!isset($_SESSION['is_logged_in'])) {
            header('Location: ' . ROOT_URL . 'blog');
        }
        $viewmodel = new BlogModel();
        $this->ReturnView($viewmodel->add(), true);
    }

}
