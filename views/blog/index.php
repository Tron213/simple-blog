<div class="container">
    <div class="row>">
        <?php if (isset($_SESSION['is_logged_in'])) : ?>
            <div class="alert panel-default text-center">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h2>Add a Blog Entry</h2>
                <p class="lead">Click the link to add a blog post</p>
                <br/>
                <a class="btn btn-success btn-share" href="<?php echo ROOT_PATH; ?>blog/add">Blog Something</a>
            </div>
        <?php endif; ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Blog</h3>
            </div>
            <?php foreach ($viewmodel as $item): ?>
                <div class="well">
                    <h3><?php echo $item['title']; ?></h3>
                    <small><?php echo $item['create_date']; ?></small>
                    <hr/>
                    <p><?php echo $item['body']; ?></p>
                    <br/>
                    <a class="btn btn-default" href="<?php echo $item['link']; ?>" target="_blank">Go To Website</a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>