<div class="container">
    <div class="row>">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Blog Entry</h3>
            </div>
            <div class="panel-body">
                <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
                    <div class="form-group">
                        <label>Share Title</label>
                        <input type="text" name="title" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Body</label>
                        <textarea name="body" rows="10" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Link</label>
                        <input type="text" name="link" class="form-control"/>
                    </div>
                    <input class="btn btn-primary" name="submit" type="submit" value="Submit"/>
                    <a class="btn btn-danger" href="<?php echo ROOT_PATH; ?>blog">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    tinymce.init({
        selector: "textarea",
        plugins: "code,link,lists,table,save,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality",
        browser_spellcheck: true
    });
</script>
