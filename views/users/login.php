<div class="container">
    <div class="row>">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" placeholder="email@rx30.com" class="form-control"
                                   value="<?php
                                   if (isset($_COOKIE["member_login"])) {
                                       echo $_COOKIE["member_login"];
                                   }
                                   ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" placeholder="Password" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label class="text-align-center" for="remember-me">Remember Me</label>
                            <input type="checkbox" name="remember"
                                   id="remember" <?php if (isset($_COOKIE["member_login"])) { ?> checked <?php } ?> />

                        </div>
                        <div class="form-group">
                            <input class="btn btn-primary" name="submitLOGIN" type="submit" value="Sign In"/>
                            <a class="btn btn-danger" href="<?php echo ROOT_PATH; ?>" role="button">Cancel</a>
                            <a class="btn btn-warning pull-right" href="<?php echo ROOT_PATH; ?>users/register"
                               role="button">Not a Member?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
