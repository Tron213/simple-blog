<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="site content">
    <meta name="author" content="Walter Prorok">

    <!-- Site Title -->
    <title>Site | Title Here</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/bootstrap.css">
    <!-- Custom Creative CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>assets/css/creative.css">
    <!-- Calendar CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>assets/css/calendar.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>assets/css/stylesheet.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/animate.min.css">
    <!-- Fav Icon -->
    <link rel="shortcut icon" href="#">
    <!-- jQuery -->
    <script src="<?php echo ROOT_PATH; ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Typeahead Script -->
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>assets/js/typeahead.min.js"></script>
    <!-- JS Tiny_MCE -->
    <script src="<?php echo ROOT_PATH; ?>assets/js/tiny_mce/tinymce.min.js"></script>
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top affix-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo ROOT_PATH; ?>">Website Name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo ROOT_URL; ?>">Home</a></li>
                <li><a href="<?php echo ROOT_URL; ?>about">About</a></li>
                <li><a href="<?php echo ROOT_URL; ?>blog">Blog</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if (isset($_SESSION['is_logged_in'])) : ?>
                    <li><a href="<?php echo ROOT_URL; ?>">Welcome: <?php echo $_SESSION['user_data']['name']; ?></a>
                    </li>
                    <li><a href="<?php echo ROOT_URL; ?>users/logout">Logout</a></li>
                <?php else : ?>
                    <li><a href="<?php echo ROOT_URL; ?>users/login">Login</a></li>
                    <li><a href="<?php echo ROOT_URL; ?>users/register">Register</a></li>
                <?php endif; ?>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container-fluid">
    <div style="margin: 60px;"></div>
    <div class="row">
        <?php Messages::display(); ?>
        <?php require($view); ?>

    </div>

</div><!-- /.container -->

<footer class="no-padding">
    <div class="footer">
        <div class="text-center">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-vimeo-square"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-apple" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-android" aria-hidden="true"></i></a>
            <p><?php echo date('Y'); ?> &copy; Company Name Here, Inc.</p>
        </div><!--End container-->
    </div><!--End footer div-->
</footer><!--End footer section-->


<!-- Bootstrap Date-Picker Plugin -->
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo ROOT_PATH; ?>assets/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="<?php echo ROOT_PATH; ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo ROOT_PATH; ?>assets/js/jquery.fittext.js"></script>
<script src="<?php echo ROOT_PATH; ?>assets/js/wow.min.js"></script>
<script src="<?php echo ROOT_PATH; ?>assets/js/datepicker.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo ROOT_PATH; ?>assets/js/creative.js"></script>

</body>

</html>
