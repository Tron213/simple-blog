<div class="container"
<div class="row">
    <div class="jumbotron">
        <div class="text-center">
            <h1>Welcome To Template</h1>
            <p class="lead">Add Lead In Title Here</p>
            <a class="btn btn-primary text-center" href="<?php echo ROOT_PATH; ?>blog/">Read Blog</a>
        </div>
    </div>
</div>
</div>

<div class="container">
    <div class="row">
        <div class="row text-center">
            <div class="col-md-4">
                <i class="fa fa-pencil-square-o fa-5x wow bounceIn text-primary" data-wow-delay=".1s"
                   aria-hidden="true"></i>
                <h2>Register</h2>
                <p>Create an Account<br/>
                    Update your Personal Profile <br/>
                    Access more Features</p>
                <a class="btn btn-primary" href="/source/simple-blog/users/register/" role="button">View details »</a>
            </div>
            <div class="col-md-4">
                <i class="fa fa-sign-in fa-5x wow bounceIn text-primary" data-wow-delay=".1s" aria-hidden="true"></i>
                <h2>Sign In</h2>
                <p>Full Fledge Member<br/>
                    Log into the site<br/>
                    Add a Blog Post</p>
                <a class="btn btn-primary" href="/source/simple-blog/users/login" role="button">View details »</a>
            </div>
            <div class="col-md-4">
                <i class="fa fa-list-alt fa-5x wow bounceIn text-primary" data-wow-delay=".1s" aria-hidden="true"></i>
                <h2>Blog</h2>
                <p>Give the Blog a Title<br/>
                    Add a body of text or short description<br/>
                    Add an optional link to the post</p>
                <a class="btn btn-primary" href="/source/simple-blog/blog/add" role="button">View details »</a>
            </div>
        </div>
    </div>
</div>
<div style="margin-bottom: 24px;"></div>