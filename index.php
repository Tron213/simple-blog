<?php

// WEB APP: site.com
// AUTHOR: Walter Prorok
// CREATED: 01/01/2017
// Start Session
session_start();
// works click BACK Button
session_cache_limiter('private_no_expire');
// No cache ABILITY to click BACK Button
header('Cache-Control: no cache');
// logout on if browser is closed
session_set_cookie_params(0);

// Include Config
require('config/config.php');

require('classes/Bootstrap.php');
require('classes/Controller.php');
require('classes/Email.php');
require('classes/Messages.php');
require('classes/Model.php');

require('controllers/about.php');
require('controllers/home.php');
require('controllers/blog.php');
require('controllers/users.php');

require('models/about.php');
require('models/home.php');
require('models/blog.php');
require('models/user.php');


$bootstrap = new Bootstrap($_GET);
$controller = $bootstrap->createController();

if ($controller) {
    $controller->executeAction();
}
